## Confluence in docker

### 0. Prerequisites
- `docker engine` 20.10+, `docker-compose` 1.29+

- In case you want to integrate `Confluence` to your existing `Jira`, refer this docs to overview:

  https://confluence.atlassian.com/doc/integrating-jira-and-confluence-2825.html

- `existing-jira/` dir contains another version of [docker-compose.yml](existing-jira/docker-compose.yml) to play with `Confluence` local deployment, in case you have an existing `Jira` instance running.

### I. Step by Step

1. For Let's Encrypt SSL certificate auto-renewal, update your `.env` like below:
   ```properties
   COMPOSE_FILE=compose.yml:compose.acme.yml
   DOMAIN_NAME=confluence.your-domain.com
   ACCOUNT_EMAIL=you@your-domain.com
   ```
   Your SSL cert will be renewed automatically every **60 days**, and configured with `OCSP Must Staple` and `Certificate Transparency`.

   Moreover, your proxy will be enabled `TLS 1.3`, `OCSP Stapling`, `Forward Secrecy` and `HSTS`.

2. Build them up:

   In case of LE cert auto-renewal, you need to start `nginx-proxy` and `nginx-proxy-acme` first:
   ```bash
   docker-compose up -d nginx-proxy-acme && docker-compose logs -f nginx-proxy nginx-proxy-acme
   ```
   Then start the other containers with:
   ```bash
   docker-compose up -d && docker-compose logs -f
   ```

3. Use below command for generating license key, assumed that your server ID is `ABCD-1234-EFGH-5678`:
   ```bash
   docker run --rm -v "${PWD}/atlassian-agent.jar:/atlassian-agent.jar" \
     openjdk:8-jre-alpine \
     java -jar atlassian-agent.jar \
     -p conf -m your@email.com -n you -o flat-earther \
     -s ABCD-1234-EFGH-5678
   ```

### II. Some helpful links to play with Confluence
1. [Linking Confluence to Jira with application link](https://confluence.atlassian.com/doc/linking-to-another-application-360677690.html)

2. [Delegating user management to Jira](https://confluence.atlassian.com/doc/connecting-to-crowd-or-jira-for-user-management-229838465.html#ConnectingtoCrowdorJiraforUserManagement-ConnectingConfluencetoJiraapplicationsforUserManagement)

3. [Disabling the Built-In User Management, delegate to Jira](https://confluence.atlassian.com/conf74/disabling-the-built-in-user-management-1003129246.html)

4. [Grant default space permissions to `jira-software-users` group also](https://confluence.atlassian.com/doc/assign-space-permissions-139460.html)

5. [Let's inform Jira users with an announcement banner](https://confluence.atlassian.com/adminjiraserver/configuring-an-announcement-banner-938846985.html)

   This should be useful, with the help of [Atlassian AUI](https://docs.atlassian.com/aui/9.0.2/docs/messages.html):
   ```html
   <div class="aui-message aui-message-success closeable">
   <p class="title">Confluence is now available!</p>
   Write something to your Jira users...
   </div>
   ```

   Or this simple one:
   ```html
   <div style="background-color: linen; border: 3px solid darkred; margin: 4px; padding: 2px; font-weight: bold; text-align: center;">
   Confluence is now available!<br/>
   You will receive a notification to authenticate with your Jira account, after which you can log in to Confluence with your Jira credential.<br/>
   Check your Application Navigator (top left of the Jira header), or access the link below:<br/>
   <a href="https://confluence.your-domain.com/">https://confluence.your-domain.com/</a>
   </div>
   ```

6. [Configuring Jira Integration in the Setup Wizard, but not working for me](https://confluence.atlassian.com/doc/configuring-jira-integration-in-the-setup-wizard-242255467.html)

7. License your plugins

   With java installed:
   ```bash
   java -jar atlassian-agent.jar \
     -m your@email.com \
     -n hino \
     -o akatsuki \
     -p com.thed.zephyr.je \
     -s ABCD-1234-EFGH-5678
   ```
   Or with docker:
   ```bash
   docker run --rm -v "${PWD}/atlassian-agent.jar:/atlassian-agent.jar" \
     openjdk:8-jre-alpine \
     java -jar atlassian-agent.jar \
     -m your@email.com \
     -n hino \
     -o akatsuki \
     -p com.thed.zephyr.je \
     -s ABCD-1234-EFGH-5678
   ```
